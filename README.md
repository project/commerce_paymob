INTRODUCTION
------------
This module integrates commerce with Paymob gateway.

INSTALLATION
------------
* Install as usual.

CONFIGURATION
-------------
* After the installation, go to `admin/commerce/config/payment-gateways` and
  add a payment gateway. Choose Paymob plugin and fill the required fields.
* You can repeat the first step to add multiple payment methods
  like Card, Bank Installments and ValU.
* That's it, now you should be able to see the credit card payment screen on
  the payment page

WEBHOOK URLs
-------------
* Open your Paymob Dashboard, go to Developers > Payment Integrations
* Edit the integration (you already set its ID in the Gateway
  creation step) and set the following:

```
Transaction processed callback = [host]/payment/notify/[payment_gateway_ID]
Transaction response callback = [host]/paymob/payment-result
```
Replace [host] with your domain with HTTPs protocol
Replace [payment_gateway_ID] with the ID of the payment gateway you
created before, you can get the ID from the URL.

RESPONSE CALLBACK
-----------------
After the transaction process is done by Paymob, the user will be redirected
to the /paymob/payment-result page with a list of query parameters:
You can check all parameters from here: https://docs.paymob.com/docs/transaction-webhooks

You can override the page twig template by copying the
/templates/payment-result.html.twig file to your theme.
Don't forget to clear the cache.

MAINTAINERS
-----------
Current maintainers:
* Abdulrahman Alhomsi (abdhomsi) - https://www.drupal.org/u/abdhomsi

TESTING Card
------------
https://docs.paymob.com/docs/card-payments

Webhook REQUEST PARAMS EXAMPLE
------------------------------
[URL]?is_voided=false&created_at=2022-09-21T23%3A04%3A35.811923&profile_id=489204&captured_amount=0&is_3d_secure=true&is_auth=false&currency=EGP&is_refunded=false&merchant_order_id=15&is_standalone_payment=true&is_void=false&source_data.type=card&source_data.sub_type=MasterCard&data.message=Approved&updated_at=2022-09-21T23%3A04%3A53.780593&integration_id=2754821&pending=false&error_occured=false&has_parent_transaction=false&is_refund=false&amount_cents=100&refunded_amount_cents=0&is_capture=false&txn_response_code=APPROVED&source_data.pan=2346&merchant_commission=0&id=58641072&order=69844461&hmac=7586b443b433a62a185ad7afc8ff73cfe5c3cea42d64d1b105c126743428bfa3ef26a2de286fdb800e32260b13e89852f3d0beb9cf966138f75608e2efa052ce&acq_response_code=00&success=true&owner=804653
