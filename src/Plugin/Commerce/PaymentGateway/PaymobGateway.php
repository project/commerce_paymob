<?php

namespace Drupal\commerce_paymob\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsNotificationsInterface;
use Drupal\commerce_paymob\Services\PaymobHttpClient;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Paymob payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "paymob",
 *   label = "Paymob",
 *   display_label = "Paymob",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_paymob\PluginForm\PaymobIframePaymentForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa"
 *   },
 *   js_library = "commerce_paymob/form",
 *   requires_billing_information = FALSE,
 * )
 */
class PaymobGateway extends OffsitePaymentGatewayBase implements SupportsNotificationsInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\commerce_paymob\Services\PaymobHttpClient
   */
  protected PaymobHttpClient $paymobHttpClient;

  /**
   * Constructs a new PaymentGatewayBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minor_units_converter
   *   The minor units converter.
   * @param \Drupal\commerce_paymob\Services\PaymobHttpClient $paymobHttpClient
   *   The Paymob HTTP client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, MinorUnitsConverterInterface $minor_units_converter, PaymobHttpClient $paymobHttpClient) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);
    $this->paymobHttpClient = $paymobHttpClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('commerce_paymob.http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_key' => '',
      'integration_id' => '',
      'iframe_id' => '',
      'hmac' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['api_key'] = [
      '#type' => 'textarea',
      '#title' => $this->t('API key'),
      '#description' => $this->t('The Paymob API key.'),
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
    ];

    $form['integration_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Integration ID'),
      '#description' => $this->t('The integration ID from the Paymob.'),
      '#default_value' => $this->configuration['integration_id'],
      '#required' => TRUE,
    ];

    $form['iframe_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Iframe ID'),
      '#description' => $this->t('The ID of the iframe to be used for this payment gatway.'),
      '#default_value' => $this->configuration['iframe_id'],
      '#required' => TRUE,
    ];

    $form['hmac'] = [
      '#type' => 'textfield',
      '#title' => $this->t('HMAC'),
      '#description' => $this->t('The HMAC encryption string to be used to validate the callback requests.'),
      '#default_value' => $this->configuration['hmac'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['api_key'] = $values['api_key'];
    $this->configuration['integration_id'] = $values['integration_id'];
    $this->configuration['iframe_id'] = $values['iframe_id'];
    $this->configuration['hmac'] = $values['hmac'];
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {

    $postData = json_decode($request->getContent());
    $postData = $postData->obj;

    // Validate the HMAC.
    $hmac_validated = $this->paymobHttpClient->validateHmac($request->query->all(), $this->configuration['hmac']);
    if (!$hmac_validated) {
      $this->logger()->error(
        $this->t(
          'The HMAC is wrong for order @order when processing the transaction.',
          ['@order' => $request->query->get('merchant_order_id')]
        )
      );
      return new JsonResponse([]);
    }

    $success = $postData->success;
    $paymob_order_id = $postData->order->id;
    $merchant_order_id = $postData->order->merchant_order_id;
    /**
    * @var \Drupal\commerce_order\Entity\Order $order
    */
    $order = Order::load($merchant_order_id);
    if ($success && $order && !$order->isPaid()) {

      // Validate the remote ID.
      if ($order->getData('paymob_remote_id') !== $paymob_order_id) {
        return new JsonResponse(['Wrong paymob_remote_id.']);
      }

      $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
      $payment = $payment_storage->create(
        [
          'state' => 'completed',
          'amount' => $order->getTotalPrice(),
          'payment_gateway' => $order->getData('paymob_gateway_id'),
          'order_id' => $order->id(),
          // Remote order ID - Remote Tnx ID.
          'remote_id' => $paymob_order_id . '-' . $postData->id,
          'remote_state' => 'paid',
        ]
      );
      $payment->save();

      $this->logger()->info(
        $this->t(
          'Saving Payment information for order @order',
          ['@order' => $order->id()]
        )
      );
    }
    else {
      $this->logger()->error(
        $this->t(
          "Something went wrong, can't process the transaction for order @order.",
          ['@order' => $request->query->get('merchant_order_id')]
        )
      );
    }
    return new JsonResponse([]);
  }

  /**
   * {@inheritdoc}
   */
  private function logger(): LoggerInterface {
    return \Drupal::logger('commerce_paymob');
  }

}
