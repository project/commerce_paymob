<?php

namespace Drupal\commerce_paymob\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Http\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The redirect page of transaction response.
 */
class TransactionResponseRedirect extends ControllerBase {

  /**
   * The cart provider.
   *
   * @var \Drupal\Core\Http\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Constructs a new CartController object.
   *
   * @param \Drupal\Core\Http\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('request_stack')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function content() {
    // Params sent from Paymob will be available in the template.
    // Check all params: https://docs.paymob.com/docs/transaction-callbacks.
    $params = $this->requestStack->getCurrentRequest()->query->all();
    return [
      '#theme' => 'payment_result',
      '#params' => $params,
    ];
  }

}
