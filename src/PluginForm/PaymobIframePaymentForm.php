<?php

namespace Drupal\commerce_paymob\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Renders the iFrame of Paymob payment method.
 */
class PaymobIframePaymentForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /**
     * @var \Drupal\commerce_payment\Entity\PaymentInterface $payment
     */
    $payment = $this->entity;

    /**
     * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin
     */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();
    /**
     * @var \Drupal\commerce_order\Entity\Order $order
     */
    $order = $payment->getOrder();

    // Create new order if it's not created before then create a payment.
    $paymobHttpClient = \Drupal::service('commerce_paymob.http_client');
    $paymob_remote_id = $order->getData('paymob_remote_id');
    if (!$paymob_remote_id) {
      $paymob_data = $paymobHttpClient->createOrder($order, $configuration['api_key'], $configuration['integration_id']);
      // Set remote id in order data.
      $order->setData('paymob_remote_id', $paymob_data['paymob_order_id']);
      $order->setData('paymob_gateway_id', $payment->getPaymentGateway()->id());
      $order->save();
      $payment_token = $paymob_data['payment_token'];
    }
    else {
      $auth_token = $paymobHttpClient->getToken($configuration['api_key']);
      $payment_token = $paymobHttpClient->createPayment($order, $auth_token, $paymob_remote_id, $configuration['integration_id']);
    }

    $form['iframe'] = [
      '#type' => 'inline_template',
      '#template' => '<iframe id="paymob-payment-iframe" width="100%" height="500px" src="{{ url }}"></iframe>',
      '#context' => [
        'url' => "https://accept.paymobsolutions.com/api/acceptance/iframes/{$configuration['iframe_id']}?payment_token={$payment_token}",
      ],
    ];

    return $form;
  }

}
