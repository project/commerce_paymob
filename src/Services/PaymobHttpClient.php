<?php

namespace Drupal\commerce_paymob\Services;

use Drupal\commerce_order\Entity\Order;
use GuzzleHttp\ClientInterface;

/**
 * Defines the Paymob Http client.
 */
class PaymobHttpClient {

  /**
   * Guzzle Http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private ClientInterface $httpClient;

  /**
   * Creates a new PaymobHttpClient object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The Guzzle Http client.
   */
  public function __construct(
        ClientInterface $client
    ) {
    $this->httpClient = $client;
  }

  /**
   * Gets auth token from Paymob.
   *
   * @param string $api_key
   *   The API Key.
   *
   * @return string
   *   Token.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getToken(string $api_key): string {
    // @todo store the token using state API for 1 hour to avoid many requests.
    $response = $this->httpClient->request(
      'POST',
      'https://accept.paymob.com/api/auth/tokens',
      [
        'json' => [
          'api_key' => $api_key,
        ],
        'headers' => ['Content-Type' => 'application/json'],
      ]
    );
    $response = json_decode($response->getBody());
    return $response->token;
  }

  /**
   * Get a PayMob order.
   *
   * @param string $order_id
   *   The accept order ID.
   * @param string $api_key
   *   The API key.
   *
   * @return array
   *   The order data.
   */
  public function getOrder(string $order_id, string $api_key): array {
    $response = $this->httpClient->request(
      'GET',
      "https://accept.paymobsolutions.com/api/ecommerce/orders/{$order_id}?token={$this->getToken($api_key)}"
    );
    $order = json_decode($response->getBody());
    return (array) $order;
  }

  /**
   * Creates new order in Paymob.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The order.
   * @param string $api_key
   *   The Paymob API Key.
   * @param string $integration_id
   *   The Paymob Integration ID.
   *
   * @return array
   *   An array with Paymob order ID and the payment token.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function createOrder(Order $order, string $api_key, string $integration_id): array {

    $order_items = [];
    foreach ($order->getItems() as $orderItem) {
      $order_items[] = [
        'name' => $orderItem->getPurchasedEntity()->getSku(),
        'amount_cents' => $this->convertToCents($orderItem->getUnitPrice()->getNumber()),
        'description' => $orderItem->getTitle(),
        'quantity' => $orderItem->getQuantity(),
      ];
    }

    // @todo support shipping.
    $auth_token = $this->getToken($api_key);
    $order_data = [
      "auth_token" => $auth_token,
      "delivery_needed" => "false",
      "amount_cents" => $this->convertToCents($order->getTotalPrice()->getNumber()),
      "currency" => $order->getTotalPrice()->getCurrencyCode(),
      "merchant_order_id" => $order->id(),
      "items" => $order_items,
    ];

    $response = $this->httpClient->request(
      'POST',
      'https://accept.paymob.com/api/ecommerce/orders',
      [
        'json' => $order_data,
        'headers' => ['Content-Type' => 'application/json'],
      ]
    );
    $order_response_data = json_decode($response->getBody());

    $paymob_order_id = $order_response_data->id;

    // Create payment for this created order.
    $payment_token = $this->createPayment($order, $auth_token, $paymob_order_id, $integration_id);
    return [
      'paymob_order_id' => $paymob_order_id,
      'payment_token' => $payment_token,
    ];
  }

  /**
   * Creates payment in Paymob.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   Commerce Order.
   * @param string $auth_token
   *   The auth token received from Paymob.
   * @param int $paymob_order_id
   *   The Paymob order ID.
   * @param string $integration_id
   *   The Paymob integration ID.
   *
   * @return string
   *   The payment key.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function createPayment(Order $order, string $auth_token, int $paymob_order_id, string $integration_id): string {
    $billingAddress = $order->getBillingProfile()->get('address')->first();

    $payment_data = [
      "auth_token" => $auth_token,
      "amount_cents" => $this->convertToCents($order->getTotalPrice()->getNumber()),
      "expiration" => 3600,
      "order_id" => $paymob_order_id,
      "billing_data" => [
        "apartment" => "NA",
        "email" => $order->getEmail(),
        "floor" => "NA",
        "first_name" => $billingAddress->getGivenName(),
        "last_name" => $billingAddress->getFamilyName(),
        "street" => $billingAddress->getAddressLine1(),
        "building" => "NA",
        "phone_number" => "NA",
        "shipping_method" => "NA",
        "postal_code" => $billingAddress->getPostalCode() ? $billingAddress->getPostalCode() : 'NA',
        "city" => $billingAddress->getLocality(),
        "country" => $billingAddress->getCountryCode(),
        "state" => $billingAddress->getAdministrativeArea(),
      ],
      "currency" => $order->getTotalPrice()->getCurrencyCode(),
      "integration_id" => $integration_id,
      "lock_order_when_paid" => "true",
    ];

    \Drupal::moduleHandler()->invokeAll(
      'commerce_paymob_alter_payment_data',
      [
        $order,
        &$payment_data,
      ]
    );

    $response = $this->httpClient->request(
      'POST',
      'https://accept.paymob.com/api/acceptance/payment_keys',
      [
        'json' => $payment_data,
        'headers' => ['Content-Type' => 'application/json'],
      ]
    );
    $response_data = json_decode($response->getBody());
    return $response_data->token;
  }

  /**
   * Validates the HMAC.
   *
   * @param array $data
   *   The response query params.
   * @param string $hmac
   *   The HMAC to compare.
   *
   * @return bool
   *   The status of the validation.
   */
  public function validateHmac(array $data, string $hmac): bool {
    $received_hmac = $data['hmac'];
    $data['order.id'] = $data['order'];
    $data['source_data.pan'] = $data['source_data_pan'];
    $data['source_data.sub_type'] = $data['source_data_sub_type'];
    $data['source_data.type'] = $data['source_data_type'];
    $considered_keys = [
      'amount_cents',
      'created_at',
      'currency',
      'error_occured',
      'has_parent_transaction',
      'id',
      'integration_id',
      'is_3d_secure',
      'is_auth',
      'is_capture',
      'is_refunded',
      'is_standalone_payment',
      'is_voided',
      'order.id',
      'owner',
      'pending',
      'source_data.pan',
      'source_data.sub_type',
      'source_data.type',
      'success',
    ];
    $final_data = [];
    foreach ($data as $key => $value) {
      if (in_array($key, $considered_keys)) {
        $final_data[$key] = $value;
      }
    }
    ksort($final_data);
    $final_data = implode('', $final_data);
    $generated_hmac = hash_hmac('sha512', $final_data, $hmac);

    return (bool) $generated_hmac == $received_hmac;
  }

  /**
   * Converts amount to cents.
   *
   * @param float $amount
   *   The amount to convert.
   *
   * @return int
   *   The converted amount in cents.
   */
  private function convertToCents(float $amount): int {
    return number_format((float) $amount * 100., 0, '.', '');
  }

}
