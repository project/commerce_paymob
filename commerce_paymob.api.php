<?php

/**
 * @file
 * Hooks provided by the Commerce Paymob module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the payment data before posting it to Paymob.
 *
 * @param \Drupal\commerce_order\Entity\Order $order
 *   The commerce Order.
 * @param array $payment_data
 *   The payment data array.
 */
function hook_commerce_paymob_alter_payment_data(\Drupal\commerce_order\Entity\Order $order, array &$payment_data) {
  $phone_number = $order->getBillingProfile()->get('field_phone')->getValue();
  if ($phone_number) {
    $payment_data['billing_data']['phone_number'] = $phone_number[0]['value'];
  }
}

/**
 * @} End of "addtogroup hooks".
 */
